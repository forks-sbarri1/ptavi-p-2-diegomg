
# ejercicio 8

import sys
import computecount

def ejec_lines(line):
    obj = line.rstrip().split(',')
    ComputeC = computecount.Compute()

    if len(obj) == 1:
        ComputeC.default = int(obj[0])
        print("Default: " + (obj[0]))

    elif len(obj) == 2:
        if obj[0] == "power":
            print(ComputeC.power(float(obj[1]), ComputeC.default))
        elif obj[0] == "log":
            print(ComputeC.log(float(obj[1]), ComputeC.default))
        else:
            print("BAD FORMAT")

    elif len(obj) == 3:
        if obj[0] == "power":
            print(ComputeC.power(float(obj[1]), float(obj[2])))
        elif obj[0] == "log":
            print(ComputeC.log(float(obj[1]), float(obj[2])))
        else:
            print("BAD FORMAT")
    else:
        print("BAD FORMAT")

def process_csv(fichero):
    with open(fichero, 'r') as file:
        for line in file:
            try:
                ejec_lines(line)
            except (ValueError, KeyError):
                print("BAD FORMAT")


if __name__ == "__main__":
    try:
        fichero = sys.argv[1]
    except IndexError:
        print("Error: Execute with the name of file to process as argument")
        sys.exit(-1)
    try:
        process_csv(fichero)
    except FileNotFoundError:
        print(f"Error: File {fichero} does not exist")
        sys.exit(-1)


