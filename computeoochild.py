
# Ejercicio 6

import computeoo
import sys
r1 = computeoo.Compute

class ComputeChild():
    def __init__(self, x):
        self.x = x

    def set_def(self):
        if x <= 0:
            raise ValueError("Number must be > 0")
        elif x == None:
            return computeoo.Compute().default
        else:
            return x

    def get_def(self):
        return ComputeChild.set_def(x)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")
    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:

        num2 = computeoo.Compute().default
    else:
        x = float(sys.argv[3])
        num2 = ComputeChild.get_def(x)


    if sys.argv[1] == "power":
        result = r1.power(num, num2)
    elif sys.argv[1] == "log":
        result = r1.log(num, num2)
    else:
        sys.exit('Operand should be power or log')

    print(result)

