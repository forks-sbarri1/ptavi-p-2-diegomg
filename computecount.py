
# Ejercicio 7

# Ejercicio 7

import math
import sys

class Compute():
    count = 0
    def __init__(self, default=2):
        self.default = default

    def power(self, num, num2=2):
        self.count = self.count + 1
        return num ** num2

    def log(self, num, num2=2):
        self.count = self.count + 1
        return math.log(num, num2)

    def set_def(self, num2):
        self.count = self.count + 1
        if len(sys.argv) == 4:
            self.default = int(sys.argv[3])
        elif num2 <= 0:
                raise ValueError("Number must be > 0")

    def get_def(self):
        self.count = self.count + 1
        return self.set_def()



